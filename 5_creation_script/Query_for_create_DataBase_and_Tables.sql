USE master;
--deleting DBD_Sales database=================================================================================database
if DB_ID('DBD_Sales') is not null
	begin
		print 'deleting database DBD_Sales'
		ALTER DATABASE DBD_Sales SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
		drop database DBD_Sales
	end
go
--creating DBD_Sales database
if DB_ID('DBD_Sales') is null
	begin
		print 'creating database DBD_Sales'
		create database DBD_Sales
	end
go

use DBD_Sales
go

--========================================================================================= creating tables


--------------------------------------------------------------------------client
if (OBJECT_ID('client') is not null)
	begin
		print'deleting table client'
		drop table client
	end
go
if (OBJECT_ID('client') is null)
	begin
		print 'creating table client'
		create table client
		(
			id bigint identity (1,1) primary key,
			[name] varchar(30),
			surname varchar(30),
			phone_number varchar(30) unique,
			address_id bigint,
			passport_id uniqueidentifier unique,
			email nvarchar(320),
			skype varchar(100),
			discount_card_no uniqueidentifier
		)
	end
go

--------------------------------------------------------------------------discount_card
if (OBJECT_ID('discount_card') is not null)
	begin
		print'deleting table discount_card'
		drop table discount_card
	end
go
if (OBJECT_ID('discount_card') is null)
	begin
		print 'creating table discount_card'
		create table discount_card
		(
			id uniqueidentifier primary key,
			discount_amount tinyint,
			[start_date] date
		)
	end
go

--------------------------------------------------------------------------address
if (OBJECT_ID('address') is not null)
	begin
		print'deleting table address'
		drop table [address]
	end
go
if (OBJECT_ID('address') is null)
	begin
		print 'creating table address'
		create table [address]
		(
			id bigint identity(1,1) primary key,
			country_id int,
			city varchar(30),
			street varchar(50),
			building varchar(10),
			appartment smallint,
			zip_code varchar(10)
		)
	end
go

--------------------------------------------------------------------------country
if (OBJECT_ID('country') is not null)
	begin
		print'deleting table country'
		drop table country
	end
go
if (OBJECT_ID('country') is null)
	begin
		print 'creating table country'
		create table country
		(
			id int identity(1,1) primary key,
			[name] varchar(30)
		)
	end
go

--------------------------------------------------------------------------film
if (OBJECT_ID('film') is not null)
	begin
		print'deleting table film'
		drop table film
	end
go
if (OBJECT_ID('film') is null)
	begin
		print 'creating table film'
		create table film
		(
			id int identity(1,1) primary key,
			[name] varchar(30),
			[year] date,
			duration time,
			budget money,
			[description] varchar(500),
			rating tinyint
		)
	end
go

--------------------------------------------------------------------------director
if (OBJECT_ID('director') is not null)
	begin
		print'deleting table director'
		drop table director
	end
go
if (OBJECT_ID('director') is null)
	begin
		print 'creating table director'
		create table director
		(
			id int identity(1,1) primary key,
			[name] varchar(30),
			surname varchar(30),
			film_id int
		)
	end
go

--------------------------------------------------------------------------actor
if (OBJECT_ID('actor') is not null)
	begin
		print'deleting table actor'
		drop table actor
	end
go
if (OBJECT_ID('actor') is null)
	begin
		print 'creating table actor'
		create table actor
		(
			id int identity(1,1) primary key,
			[name] varchar(30),
			surname varchar(30),
			gender char,
			birthdate date,
			award_id int
		)
	end
go

--------------------------------------------------------------------------award
if (OBJECT_ID('award') is not null)
	begin
		print'deleting table award'
		drop table award
	end
go
if (OBJECT_ID('award') is null)
	begin
		print 'creating table award'
		create table award
		(
			id int identity(1,1) primary key,
			[name] varchar(30) unique
		)
	end
go

--------------------------------------------------------------------------category
if (OBJECT_ID('category') is not null)
	begin
		print'deleting table category'
		drop table category
	end
go
if (OBJECT_ID('category') is null)
	begin
		print 'creating table category'
		create table category
		(
			id int identity(1,1) primary key,
			[name] varchar(20) unique
		)
	end
go


--------------------------------------------------------------------------genre
if (OBJECT_ID('genre') is not null)
	begin
		print'deleting table genre'
		drop table genre
	end
go
if (OBJECT_ID('genre') is null)
	begin
		print 'creating table genre'
		create table genre
		(
			id int identity(1,1) primary key,
			[name] varchar(20) unique
		)
	end
go

--------------------------------------------------------------------------production
if (OBJECT_ID('production') is not null)
	begin
		print'deleting table production'
		drop table production
	end
go
if (OBJECT_ID('production') is null)
	begin
		print 'creating table production'
		create table production
		(
			id tinyint identity(1,1) primary key,
			[name] varchar(30) unique
		)
	end
go

--------------------------------------------------------------------------language
if (OBJECT_ID('language') is not null)
	begin
		print'deleting table language'
		drop table [language]
	end
go
if (OBJECT_ID('language') is null)
	begin
		print 'creating table language'
		create table [language]
		(
			id smallint identity(1,1) primary key,
			[name] varchar(20)
		)
	end
go

--------------------------------------------------------------------------film_director
if (OBJECT_ID('film_director') is not null)
	begin
		print'deleting table film_director'
		drop table film_director
	end
go
if (OBJECT_ID('film_director') is null)
	begin
		print 'creating table film_director'
		create table film_director
		(
			id int identity(1,1) primary key,
			director_id int,
			film_id int 
		)
	end
go

--------------------------------------------------------------------------film_country
if (OBJECT_ID('film_country') is not null)
	begin
		print'deleting table film_country'
		drop table film_country
	end
go
if (OBJECT_ID('film_country') is null)
	begin
		print 'creating table film_country'
		create table film_country
		(
			id int identity(1,1) primary key,
			country_id int,
			film_id int
		)
	end
go

--------------------------------------------------------------------------film_actor
if (OBJECT_ID('film_actor') is not null)
	begin
		print'deleting table film_actor'
		drop table film_actor
	end
go
if (OBJECT_ID('film_actor') is null)
	begin
		print 'creating table film_actor'
		create table film_actor
		(
			id int identity(1,1) primary key,
			actor_id int,
			film_id int,
			[name] varchar(30),
			surname varchar(30),
			birth_date date
		)
	end
go

--------------------------------------------------------------------------film_production
if (OBJECT_ID('film_production') is not null)
	begin
		print'deleting table film_production'
		drop table film_production
	end
go
if (OBJECT_ID('film_production') is null)
	begin
		print 'creating table film_production'
		create table film_production
		(
			id smallint identity(1,1) primary key,
			production_id tinyint,
			film_id int
		)
	end
go

--------------------------------------------------------------------------film_audio
if (OBJECT_ID('film_audio') is not null)
	begin
		print'deleting table film_audio'
		drop table film_audio
	end
go
if (OBJECT_ID('film_audio') is null)
	begin
		print 'creating table film_audio'
		create table film_audio
		(
			id int identity(1,1) primary key,
			film_id int,
			lang_id smallint
		)
	end
go

--------------------------------------------------------------------------film_subtitles
if (OBJECT_ID('film_subtitles') is not null)
	begin
		print'deleting table film_subtitles'
		drop table film_subtitles
	end
go
if (OBJECT_ID('film_subtitles') is null)
	begin
		print 'creating table film_subtitles'
		create table film_subtitles
		(
			id int identity(1,1) primary key,
			film_id int,
			lang_id smallint
		)
	end
go

--------------------------------------------------------------------------disk
if (OBJECT_ID('disk') is not null)
	begin
		print'deleting table disk'
		drop table [disk]
	end
go
if (OBJECT_ID('disk') is null)
	begin
		print 'creating table disk'
		create table [disk]
		(
			id uniqueidentifier primary  key,
			format_id int,
			[3d_option] bit,
			publisher_id smallint,
			sale_price money,
			daily_rent_price smallmoney
		)
	end
go

--------------------------------------------------------------------------disk_film
if (OBJECT_ID('disk_film') is not null)
	begin
		print'deleting table disk_film'
		drop table disk_film
	end
go
if (OBJECT_ID('disk_film') is null)
	begin
		print 'creating table disk_film'
		create table disk_film
		(
			id int identity(1,1) primary key,
			disk_id uniqueidentifier,
			film_id int
		)
	end
go

--------------------------------------------------------------------------format
if (OBJECT_ID('format') is not null)
	begin
		print'deleting table format'
		drop table [format]
	end
go
if (OBJECT_ID('format') is null)
	begin
		print 'creating table format'
		create table [format]
		(
			format_id int identity(1,1) primary key,
			[name] varchar(20) unique
		)
	end
go

--------------------------------------------------------------------------publisher
if (OBJECT_ID('publisher') is not null)
	begin
		print'deleting table publisher'
		drop table publisher
	end
go
if (OBJECT_ID('publisher') is null)
	begin
		print 'creating table publisher'
		create table publisher
		(
			id smallint identity(1,1) primary key,
			[name] varchar(20)
		)
	end
go

--------------------------------------------------------------------------order
if (OBJECT_ID('order') is not null)
	begin
		print'deleting table order'
		drop table [order]
	end
go
if (OBJECT_ID('order') is null)
	begin
		print 'creating table order'
		create table [order]
		(
			id int identity(1,1) primary key,
			client_id bigint,
			delivery_id int 
		)
	end
go

--------------------------------------------------------------------------order_disk
if (OBJECT_ID('order_disk') is not null)
	begin
		print'deleting table order_disk'
		drop table order_disk
	end
go
if (OBJECT_ID('order_disk') is null)
	begin
		print 'creating table order_disk'
		create table order_disk
		(
			id int identity(1,1) primary key,
			order_id int,
			disk_id uniqueidentifier 
		)
	end
go

--------------------------------------------------------------------------order_address
if (OBJECT_ID('order_address') is not null)
	begin
		print'deleting table order_address'
		drop table order_address
	end
go
if (OBJECT_ID('order_address') is null)
	begin
		print 'creating table order_address'
		create table order_address
		(
			id int identity(1,1) primary key,
			order_id int,
			address_id bigint  
		)
	end
go

--------------------------------------------------------------------------delivery
if (OBJECT_ID('delivery') is not null)
	begin
		print'deleting table delivery'
		drop table delivery
	end
go
if (OBJECT_ID('delivery') is null)
	begin
		print 'creating table delivery'
		create table delivery
		(
			id int identity(1,1) primary key,
			delivery_date date,
			comment varchar(100),
			delivery_type_id tinyint 
		)
	end
go

--------------------------------------------------------------------------delivery_type
if (OBJECT_ID('delivery_type') is not null)
	begin
		print'deleting table delivery_type'
		drop table delivery_type
	end
go
if (OBJECT_ID('delivery_type') is null)
	begin
		print 'creating table delivery_type'
		create table delivery_type
		(
			id tinyint identity(1,1) primary key,
			[name] char(4)
		)
	end
go

--------------------------------------------------------------------------order_delivery_type
if (OBJECT_ID('order_delivery_type') is not null)
	begin
		print'deleting table order_delivery_type'
		drop table order_delivery_type
	end
go
if (OBJECT_ID('order_delivery_type') is null)
	begin
		print 'creating table order_delivery_type'
		create table order_delivery_type
		(
			id int identity(1,1) primary key,
			order_id int,
			delivery_id int
		)
	end
go

--------------------------------------------------------------------------operation
if (OBJECT_ID('operation') is not null)
	begin
		print'deleting table operation'
		drop table operation
	end
go
if (OBJECT_ID('operation') is null)
	begin
		print 'creating table operation'
		create table operation
		(
			id int identity(1,1) primary key,
			[date] smalldatetime,
			return_date smalldatetime,
			[type_id] int,  
			payment_id int
		)
	end
go

--------------------------------------------------------------------------operation_type
if (OBJECT_ID('operation_type') is not null)
	begin
		print'deleting table operation_type'
		drop table operation_type
	end
go
if (OBJECT_ID('operation_type') is null)
	begin
		print 'creating table operation_type'
		create table operation_type
		(
			id int identity(1,1) primary key,
			[name] varchar(10)
		)
	end
go

--------------------------------------------------------------------------payment
if (OBJECT_ID('payment') is not null)
	begin
		print'deleting table payment'
		drop table payment
	end
go
if (OBJECT_ID('payment') is null)
	begin
		print 'creating table payment'
		create table payment
		(
			id int identity(1,1) primary key,
			payment_type_id int,
			amount money
		)
	end
go

--------------------------------------------------------------------------payment_type
if (OBJECT_ID('payment_type') is not null)
	begin
		print'deleting table payment_type'
		drop table payment_type
	end
go
if (OBJECT_ID('payment_type') is null)
	begin
		print 'creating table payment_type'
		create table payment_type
		(
			id int identity(1,1) primary key,
			[name] varchar (11)
		)
	end
go

--------------------------------------------------------------------------shop
if (OBJECT_ID('shop') is not null)
	begin
		print'deleting table shop'
		drop table shop
	end
go
if (OBJECT_ID('shop') is null)
	begin
		print 'creating table shop'
		create table shop
		(
			id int identity(1,1) primary key,
			shop_name varchar(30),
			address_id bigint,
			http_adress varchar(320),
			email nvarchar(320),
			manager_id int,
			phone_number varchar(30),
			opening_hours varchar(15)
		)
	end
go

--------------------------------------------------------------------------manager
if (OBJECT_ID('manager') is not null)
	begin
		print'deleting table manager'
		drop table manager
	end
go
if (OBJECT_ID('manager') is null)
	begin
		print 'creating table manager'
		create table manager
		(
			id int identity(1,1) primary key,
			[name] varchar(30)
		)
	end
go

--------------------------------------------------------------------------employee
if (OBJECT_ID('employee') is not null)
	begin
		print'deleting table employee'
		drop table employee
	end
go
if (OBJECT_ID('employee') is null)
	begin
		print 'creating table employee'
		create table employee
		(
			id int identity(1,1) primary key,
			name varchar(30),
			surname varchar(30),
			position_id int,
			employment_date date,
			salary money,
			bonus_to_salary tinyint,
			email nvarchar(320),
			address_id bigint,
		)
	end
go

--------------------------------------------------------------------------position
if (OBJECT_ID('position') is not null)
	begin
		print'deleting table position'
		drop table position
	end
go
if (OBJECT_ID('position') is null)
	begin
		print 'creating table position'
		create table position
		(
			id int identity(1,1) primary key,
			[name] varchar(30)
		)
	end
go

--------------------------------------------------------------------------position_superior
if (OBJECT_ID('position_superior') is not null)
	begin
		print'deleting table position_superior'
		drop table position_superior
	end
go
if (OBJECT_ID('position_superior') is null)
	begin
		print 'creating table position_superior'
		create table position_superior
		(
			id int identity(1,1) primary key,
			position_id int,
			superior_id  int
		)
	end
go

--------------------------------------------------------------------------superior
if (OBJECT_ID('superior') is not null)
	begin
		print'deleting table superior'
		drop table superior
	end
go
if (OBJECT_ID('superior') is null)
	begin
		print 'creating table superior'
		create table superior
		(
			id int identity(1,1) primary key,
			[name] varchar(30)
		)
	end
go

--------------------------------------------------------------------------actor_award
if (OBJECT_ID('actor_award') is not null)
	begin
		print'deleting table actor_award'
		drop table actor_award
	end
go
if (OBJECT_ID('actor_award') is null)
	begin
		print 'creating table actor_award'
		create table actor_award
		(
			id int primary key identity(1,1),
			actor_id int,
			award_id int
		)
	end
go

--------------------------------------------------------------------------film_award
if (OBJECT_ID('film_award') is not null)
	begin
		print'deleting table film_award'
		drop table film_award
	end
go
if (OBJECT_ID('film_award') is null)
	begin
		print 'creating table film_award'
		create table film_award
		(
			id int primary key identity(1,1),
			award_id int,
			film_id int
		)
	end
go

--------------------------------------------------------------------------film_genre
if (OBJECT_ID('film_genre') is not null)
	begin
		print'deleting table film_genre'
		drop table film_genre
	end
go
if (OBJECT_ID('film_genre') is null)
	begin
		print 'creating table film_genre'
		create table film_genre
		(
			id int primary key identity(1,1),
			genre_id int foreign key references genre(id),
			film_id int foreign key references film(id)
		)
	end
go

--------------------------------------------------------------------------film_category
if (OBJECT_ID('film_category') is not null)
	begin
		print'deleting table film_category'
		drop table film_category
	end
go
if (OBJECT_ID('film_category') is null)
	begin
		print 'creating table film_category'
		create table film_category
		(
			id int primary key identity(1,1),
			category_id int foreign key references category(id),
			film_id int foreign key references film(id)
		)
	end
go



--====================================================================================================================  creating foreign keys

--actor_award
begin
	alter table actor_award
		ADD constraint FK_actor_award_actor_id FOREIGN KEY (actor_id) REFERENCES actor(id),
			constraint FK_actor_award_award_id FOREIGN KEY (award_id) REFERENCES award(id)
		print 'created FK for actor_award'
	end
go
--film_award
begin
	alter table film_award
		ADD constraint FK_film_award_film_id FOREIGN KEY (film_id) REFERENCES film(id),
			constraint FK_film_award_award_id FOREIGN KEY (award_id) REFERENCES award(id)
		print 'created FK for film_award'
	end
go
--film_genre
begin
	alter table film_genre
		ADD constraint FK_film_genre_film_id FOREIGN KEY (film_id) REFERENCES film(id),
			constraint FK_film_genre_genre_id FOREIGN KEY (genre_id) REFERENCES genre(id)
		print 'created FK for film_genre'
	end
go
--film_category
begin
	alter table film_category
		ADD constraint FK_film_category_film_id FOREIGN KEY (film_id) REFERENCES film(id),
			constraint FK_film_category_category_id FOREIGN KEY (category_id) REFERENCES category(id)
		print 'created FK for film_category'
	end
go
--client
begin
	alter table client
		ADD constraint FK_client_address_id FOREIGN KEY (address_id) REFERENCES [address](id),
			constraint FK_client_discount_card_no FOREIGN KEY (discount_card_no) REFERENCES discount_card(id)
		print 'created FK for client'
	end
go
--address
begin
	alter table [address]
		ADD constraint FK_address_country_id FOREIGN KEY (country_id) REFERENCES country(id)
		print 'created FK for address'
	end
go
--director
begin
	alter table director
		ADD constraint FK_director_film_id FOREIGN KEY (film_id) REFERENCES film(id)
		print 'created FK for director'
	end
go
--actor
begin
	alter table actor
		ADD constraint FK_actor_award_id FOREIGN KEY (award_id) REFERENCES award(id)
		print 'created FK for actor'
	end
go
--film_director
begin
	alter table film_director
		ADD constraint FK_film_director_director_id FOREIGN KEY (director_id) REFERENCES director(id),
			constraint FK_film_director_film_id FOREIGN KEY (film_id) REFERENCES film(id)
		print 'created FK for film_director'
	end
go
--film_country
begin
	alter table film_country
		ADD constraint FK_film_country_country_id FOREIGN KEY (country_id) REFERENCES country(id),
			constraint FK_film_country_film_id FOREIGN KEY (film_id) REFERENCES film(id)
		print 'created FK for film_country'
	end
go
--film_actor
begin
	alter table film_actor
		ADD constraint FK_film_actor_actor_id FOREIGN KEY (actor_id) REFERENCES actor(id),
			constraint FK_film_actor_film_id FOREIGN KEY (film_id) REFERENCES film(id)
		print 'created FK for film_actor'
	end
go
--film_production
begin
	alter table film_production
		ADD constraint FK_film_production_production_id FOREIGN KEY (production_id) REFERENCES production(id),
			constraint FK_film_production_film_id FOREIGN KEY (film_id) REFERENCES film(id)
		print 'created FK for film_production'
	end
go
--film_audio
begin
	alter table film_audio
		ADD constraint FK_film_audio_lang_id FOREIGN KEY (lang_id) REFERENCES [language](id),
			constraint FK_film_audio_film_id FOREIGN KEY (film_id) REFERENCES film(id)
		print 'created FK for film_audio'
	end
go
--film_subtitles
begin
	alter table film_subtitles
		ADD constraint FK_film_subtitles_lang_id FOREIGN KEY (lang_id) REFERENCES [language](id),
			constraint FK_film_subtitles_film_id FOREIGN KEY (film_id) REFERENCES film(id)
		print 'created FK for film_subtitles'
	end
go
--disk
begin
	alter table [disk]
		ADD constraint FK_disk_format_id FOREIGN KEY (format_id) REFERENCES [format](format_id),
			constraint FK_disk_publisher_id FOREIGN KEY (publisher_id) REFERENCES publisher(id)
		print 'created FK for disk'
	end
go
--disk_film
begin
	alter table disk_film
		ADD constraint FK_disk_film_disk_id FOREIGN KEY (disk_id) REFERENCES [disk](id),
			constraint FK_disk_film_film_id FOREIGN KEY (film_id) REFERENCES film(id)
		print 'created FK for disk_film'
	end
go
--order
begin
	alter table [order]
		ADD constraint FK_order_client_id FOREIGN KEY (client_id) REFERENCES client(id),
			constraint FK_order_delivery_id FOREIGN KEY (delivery_id) REFERENCES delivery(id)
		print 'created FK for order'
	end
go
--order_disk
begin
	alter table order_disk
		ADD constraint FK_order_disk_order_id FOREIGN KEY (order_id) REFERENCES [order](id),
			constraint FK_order_disk_disk_id FOREIGN KEY (disk_id) REFERENCES [disk](id)
		print 'created FK for order_disk'
	end
go
--order_address
begin
	alter table order_address
		ADD constraint FK_order_address_order_id FOREIGN KEY (order_id) REFERENCES [order](id),
			constraint FK_order_address_address_id FOREIGN KEY (address_id) REFERENCES [address](id)
		print 'created FK for order_address'
	end
go
--delivery
begin
	alter table delivery
		ADD constraint FK_delivery_delivery_type_id FOREIGN KEY (delivery_type_id) REFERENCES delivery_type([name])
		print 'created FK for delivery'
	end
go
--order_delivery_type
begin
	alter table order_delivery_type
		ADD constraint FK_order_delivery_type_order_id FOREIGN KEY (order_id) REFERENCES [order](id),
			constraint FK_order_delivery_type_delivery_id FOREIGN KEY (delivery_id) REFERENCES delivery(id)
		print 'created FK for order_delivery_type'
	end
go
--operation
begin
	alter table operation
		ADD constraint FK_operation_type_id FOREIGN KEY ([type_id]) REFERENCES operation_type(id),
			constraint FK_operation_payment_id FOREIGN KEY (payment_id) REFERENCES payment(id)
		print 'created FK for operation'
	end
go
--payment
begin
	alter table payment
		ADD constraint FK_payment_payment_type_id FOREIGN KEY (payment_type_id) REFERENCES payment_type(id)
		print 'created FK for payment'
	end
go
--shop
begin
	alter table shop
		ADD constraint FK_shop_address_id FOREIGN KEY (address_id) REFERENCES [address](id),
			constraint FK_shop_manager_id FOREIGN KEY (manager_id) REFERENCES manager(id)
		print 'created FK for shop'
	end
go
--employee
begin
	alter table employee
		ADD constraint FK_employee_position_id FOREIGN KEY (position_id) REFERENCES position(id),
			constraint FK_employee_address_id FOREIGN KEY (address_id) REFERENCES [address](id)
		print 'created FK for employee'
	end
go
--position_superior
begin
	alter table position_superior
		ADD constraint FK_position_superior_position_id FOREIGN KEY (position_id) REFERENCES position(id),
			constraint FK_position_superior_superior_id FOREIGN KEY (superior_id) REFERENCES superior(id)
		print 'created FK for position_superior'
	end
go